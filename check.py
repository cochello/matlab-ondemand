"""
This file checks if your template app is valid and ready for deployment.

We do a few things:

    - Check for required/optional sections
    - Check YAML syntax

Upon running, we wil display any issues that we encounter.
We can be configured to display errors, warnings, syntax issues, or all three.
"""

import json
import argparse

from typing import Tuple, Union

ERROR = "!["
ERRORE = "]!"
WARNING = "?["
WARNINGE = "]?"


def find_pattern(start_pattern: str, end_pattern: str, string: str) -> Union[str, None]:
    """
    Find the given pattern in the given string.

    A 'pattern' is a string enclosed by two characters.
    If the start character is '![' and the end character is ']!',
    then we would look for text enclosed within:

    ![TEXT HERE]!

    If the given pattern is found, then we would return 'TEXT HERE'.
    Otherwise, return None.

    If we encounter any malformed patterns,
    we print a warning and assume
    the end of the string is the end of the pattern.

    :param start_pattern: Start character
    :type start_pattern: str
    :param end_pattern: End character
    :type end_pattern: str
    :param string: Text to search
    :type string: str
    :return: The text enclosed in the pattern, or None if pattern not found
    :rtype: Union[str, None]
    """
    # Attempt to locate start pattern:

    start = string.find(start_pattern)

    # Maybe Found the first half, look for the second:

    end = string.find(end_pattern)

    # Ensure at least one is valid:

    if start < 0 and end < 0:

        # No valid pattern found, return None

        return None

    # Ensure end is valid, otherwise just print a warning:

    if end < 0 or end < start:

        print(" --== MALFORMED PATTERN FOUND! ==--")
        print("No end pattern found:")

        print("\n{}\n".format(string))
            
        print("Assuming end of line is end of pattern")

    if start < 0 or start > end:

        print(" --== MALFORMED PATTERN FOUND! ==--")
        print("No start pattern found:")

        print("\n{}\n".format(string))

        print("Assuming start of line is start of pattern")

        start = 0 - len(start_pattern)

    # Finally, return pattern:

    return string[start+len(start_pattern):end]


def find_file_pattern(files: list[str], start_pattern: str, stop_pattern: str) -> list[Tuple[str, str, int]]:
    """
    Find a generic pattern in a list of files.

    We iterate over the files provided,
    and then search each line for a given pattern.

    We utilize the 'find_pattern()' method to find the pattern.

    :param files: List of files to search through
    :type files: list[str]
    :param start_pattern: Start character to search for
    :type start_pattern: str
    :param stop_pattern: Stop character to search for
    :type stop_pattern: str
    :return: List of all matches, file match was found in, and line
    :rtype: list[Tuple[str, str, int]]
    """
    text: list[Tuple[str, str, int]] = []

    # Iterate over each file:

    for name in files:

        # Open the file:

        with open(name, 'r', encoding='utf-8') as file:

            lines = file.readlines()

        # Iterate over each line in the file:

        for num, line in enumerate(lines):

            # Check if pattern is present in line:

            temp = find_pattern(start_pattern, stop_pattern, line)

            if temp:

                # Found our pattern! Save it:

                text.append((temp, name, num))

    return text


def find_errors(file_names: list[str]) -> list[Tuple[str, str, int]]:
    """
    Find all errors in the given files.

    Errors are defined as text within the characters '![]!'.
    An error should have text in the middle that describes the issue.
    Here is an example error:

    ![This is an error]!

    Errors are usually in comments,
    and it's presence denotes that section must be changed.

    :param file_names: A list of files to search
    :type file_names: list[str]
    :return: A list of all errors and their line numbers
    :rtype: list[str, int]
    """
    return find_file_pattern(file_names, ERROR, ERRORE)


def find_warnings(file_names: list[str]) -> list[Tuple[str, str, int]]:
    """
    Find all warnings in the given files.

    Warnings are defined as text within the characters '?[]?'.
    A warning should have text in the middle that describes the issue.
    Here is an example warning:

    ?[This is a warning]?

    Warnings are usually in comments,
    and it's presence denotes that a section might need ot be changed.

    :param file_names: A list of files to search
    :type file_names: list[str]
    :return: A list of all warnings and their line numbers
    :rtype: list[Tuple[str, int]]
    """
    return find_file_pattern(file_names, WARNING, WARNINGE)


# import the file index:

files = json.loads(open("index.json", "r", encoding="utf-8").read())

print(files)

# Check for errors:

errs = find_errors(files['files'])

for err in errs:

    print("Found error on line [{}] in file [{}] :".format(err[2], err[1]))
    print("\n{}\n".format(err[0]))

# Check for warnings:

wars = find_warnings(files['files'])

for war in wars:

    print("Found warning on line [{}] in file [{}] :".format(war[2], war[1]))
    print("\n{}\n".format(war[0]))
