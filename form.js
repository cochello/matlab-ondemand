'use strict'

/**
 * This file describes the logic of the form page.
 * 
 * You can use this file, along with JS technologies and included methods,
 * to add logic and change the look of the input page.
 * 
 * For example, this file includes a method that ensures that the number
 * of cores selected is valid.
 * We run this check each time the 'node_type' changes.
 * This file also toggles the visibility of the advanced options,
 * showing them and hiding them based upon the value of the 'advanced_options' checkbox.
 * 
 * The world is your oyster!
 * This system gives you a lot of freedom to decide how the logic
 * of your form will play out.
 * 
 * We do provide some helper methods for easing this development process,
 * checkout the docstring for help on each one.
 * 
 * Most of the time, the handler workflow works very well for this kind of work.
 * It goes like this:
 * 
 * - Create a function that does something, like make options hidden when called
 * - Attach this function to an attribute as a change handler
 * 
 * A 'change handler' gets called whenever the value changes.
 * This allows you have logic, visibility, and other functionality
 * run whenever a value changes.
 * See the end of this file for examples of adding event handlers.
 */

/**
 * Clamp between two numbers
 *
 * @param      {number}  min     The minimum
 * @param      {number}  max     The maximum
 * @param      {number}  val     The value to clamp
 */
function clamp(min, max, val) {
  return Math.min(max, Math.max(min, val));
}

/**
 * Hide an option by form_id
 * 
 * You can use this to hide any form attribute.
 * 
 * @param      {string}    form_id  The form identifier
 *
 */
function hide_option(form_id) {
  let form_element = $(form_id);
  let parent = form_element.parent();
  parent.hide();
}

/**
 * Show an option by form_id
 *
 * You can use this to show any form attribute that has been hidden.
 * 
 * @param      {string}    form_id  The form identifier
 * 
 */
function show_option(form_id) {
  let form_element = $(form_id);
  let parent = form_element.parent();
  parent.show();
}

/**
 * Return the full ID of your attribute.
 * 
 * To do operations on your attributes, you will need the full HTML ID
 * of your option.
 * Luckily, there is a way to generate this ID using just the name of your attribute,
 * and this function does just that.
 * 
 * Simply provide the name of your attribute and this function
 * will return the HTML ID of that attribute.
 * 
 * @param {string} id The name of your attribute
 * @returns The full HTML form ID
 */
function get_form_id(id) {

  return '#batch_connect_session_context_' + id;
}

/**
 * Attaches a change handler to the given attribute.
 * 
 * This will set the given function as a change handler to the
 * given attribute.
 * When this attribute changes, your function will be called!
 * 
 * This can be very useful for visibility and logic checks
 * after an attribute has been altered.
 * 
 * @param {string} id ID of the attribute to attach to the handler to
 * @param {Function} hand Handler to attach
 */
function register_handler(id, hand) {
  let form = $(id);
  form.change(hand);
}

/**
 * Below are methods that provide functionality to the default template options.
 * 
 * It is not recommended to alter this section, 
 * as it may break the default options!
 */

/**
 * Fix num cores, allowing blanks to remain
 */
function fix_num_cores() {
  let node_type_input = $('#batch_connect_session_context_node_type');
  let num_cores_input = $('#batch_connect_session_context_num_cores');

  set_ppn_by_node_type(node_type_input, num_cores_input);
}

/**
 * Sets the ppn by node type.
 *
 * @param      {element}  node_type_input  The node type input
 * @param      {element}  num_cores_input  The number cores input
 */
function set_ppn_by_node_type(node_type_input, num_cores_input) {
  let data = node_type_input.find(':selected').data();

  num_cores_input.attr('max', data.maxPpn);
  num_cores_input.attr('min', data.minPpn);

  if(num_cores_input.val() == '') {
    return;
  }
  
  // Clamp value between min and max
  num_cores_input.val(
    clamp(data.minPpn, data.maxPpn, num_cores_input.val())
  );
}

/**
 * Toggle the visibility of a advanced options form group
 * 
 * We determine if they should be visible by using the value
 * from the 'advanced_options' checkbox, showing them if it is checked,
 * and hiding them if not.
 * 
 * You can call this function at any time to refresh the UI.
 * We are called automatically when necessary by default.
 *
 * @param      {string}    form_id  The form identifier
 */
function toggle_visibility_of_advanced_options(form_id) {
  let form_element = $(form_id);
  var advanced_options_input = document.getElementById('batch_connect_session_context_advanced_options');
  let parent = form_element.parent();

  if(advanced_options_input.checked) {
    parent.show();
  } else {
    if (form_id == '#batch_connect_session_context_node_type') {
      form_element.val('any');
    } else {
      form_element.val('');
    }
    parent.hide();
  }
}

/**
 * Toggle the visibility of advanced options.
 * 
 * If you added any advanced options, and want them to be hidden when not needed,
 * be sure to add their IDs here!
 * You can utilize the 'get_form_id()' method to get the ID of your attribute.
 * 
 * Simply add your ID to the end of the list and your attribute will be auto-hidden.
 */
function toggle_advanced_options_visibility() {
  var advanced_fields = ['#batch_connect_session_context_bc_account',
                         '#batch_connect_session_context_num_gpus',
                         '#batch_connect_session_context_node_type',
                         '#batch_connect_session_context_num_tasks',
                         '#batch_connect_session_context_qos',
                         '#batch_connect_session_context_reservation',];
  
  advanced_fields.forEach(toggle_visibility_of_advanced_options);
}

/**
 * Sets the change handler for the node_type select.
 */
function set_node_type_change_handler() {
  let node_type_input = $('#batch_connect_session_context_node_type');
  node_type_input.change(node_type_change_handler);
}

/**
 * Sets the change handler for advanced_options check box
 */
function set_advanced_options_change_handler() {
  let advanced_options_input = $('#batch_connect_session_context_advanced_options');
  advanced_options_input.change(advanced_options_change_handler);
}

/**
 * Sets the change handler for Jupyter location
 *
 */
function set_jupyter_location_change_handler() {
  let jupyter_location_input = $('#batch_connect_session_context_jupyter_location');
  jupyter_location_input.change(jupyter_location_change_handler);
}

/**
 * Update UI when node_type changes
 */
function node_type_change_handler() {
  fix_num_cores();
}

/**
 * Update UI when advanced_options changes
 */
function advanced_options_change_handler() {
  toggle_advanced_options_visibility();
  fix_num_cores();
}

// END BUILTIN METHODS

/**
 * Main
 */

// Set controls to align with the values of the last session context
fix_num_cores();
toggle_advanced_options_visibility();

// Install event handlers
set_node_type_change_handler();
set_advanced_options_change_handler();
set_jupyter_location_change_handler();

// You should add any custom event handlers below this line:
